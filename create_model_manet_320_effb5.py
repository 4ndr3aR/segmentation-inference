def create_model(device='cuda:0'):
    import segmentation_models_pytorch as smp
    
    ENCODER = 'efficientnet-b5'
    ENCODER_WEIGHTS = 'imagenet'
    CLASSES = ['BW','C','P']
    ACTIVATION = None # could be None for logits or 'softmax2d' for multiclass segmentation

    # create segmentation model with pretrained encoder
    net = smp.MAnet(
        encoder_name=ENCODER,
        encoder_depth=5,
        encoder_weights=ENCODER_WEIGHTS,
        decoder_use_batchnorm=True, 
        decoder_channels=(256, 128, 64, 32, 16),
        decoder_pab_channels=64,
        in_channels=3,
        classes=len(CLASSES), 
        activation=ACTIVATION
    )

    if not device:
        device = torch.device('cuda:1' if torch.cuda.is_available() else 'cpu')
        
    model=net.to(device)
    preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)
    
    return model
