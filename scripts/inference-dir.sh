#!/bin/bash

if [ -z "$1" ] ; then
	echo
	echo "No model name supplied."
	echo
	exit 1
fi
if [ -z "$2" ] ; then
	echo
	echo "No input directory supplied."
	echo
	exit 1
fi

model=$1
dir=$2
outdir=$3

model_dir="models"

if [ -z "$outdir" ] ; then
	outdir=$dir
fi

if [ "$model" == 'manet' ] ; then
	arch='--arch create_model_manet_320_effb5'
	chkpt="$model_dir/weights_smp_manet_320_effb5_cecv_01_last.pt"
elif [ "$model" == 'unet' ] ; then
	arch='--arch create_model_unet_320_effb0'
	chkpt="$model_dir/weights_smp_unet_320_effb0_cecv_01_metric.pt"
elif [ "$model" == 'unetpp' ] ; then
	arch='--arch create_model_unetpp_320_effb5'
	chkpt="$model_dir/weights_smp_unetpp_320_effb5_cecv_01_loss.pt"
elif [ "$model" == 'baseline' ] ; then
	arch=''
	chkpt='pothole-segmentron-deeplabv3+-resnet101-basic-data-aug-img_size-540-540-2a-2021-11-04_11.02.00-WD-0.0001-BS-4-LR-0.0001-0.001-epoch-16-valid_loss-0.0402.pth'
else
	echo 'Unsupported arch'
	exit 1
fi

echo '-------------------------------------------------'
echo "Model : $model"
echo '-------------------------------------------------'
echo "Arch  : $arch"
echo "Chkpt : $chkpt"
echo "Dir   : $dir"
echo "OutDir: $outdir"
echo '-------------------------------------------------'

sleep 5

cd ..

./inference_v2_seg.py $arch --model $chkpt --dataset-dir ./fake-dataset --class a,b,c --output-dir $outdir/$model --dir $dir

