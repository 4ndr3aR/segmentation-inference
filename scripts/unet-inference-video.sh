#!/bin/bash

if [ -z "$1" ] ; then
	echo
	echo "No video file supplied."
	echo
	exit 1
fi

video=$1
outdir=$2

if [ -z "$outdir" ] ; then
	outdir=`dirname $video`
	mkdir -p "$outdir"
fi

model_dir="models"
classes='asphalt[0;0;0],pothole[0;0;255],crack[255;0;0]'

cd ..

./inference_v2_seg.py --arch create_model_unet_320_effb0 --model "$model_dir"/weights_smp_unet_320_effb0_cecv_01_metric.pt --dataset-dir ./fake-dataset --classes "$classes" --output-dir "$outdir"/unet --video "$video"
