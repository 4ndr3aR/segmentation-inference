#!/bin/bash

if [ -z "$1" ] ; then
	echo
	echo "No video file supplied."
	echo
	exit 1
fi

video=$1
outdir=$2

if [ -z "$outdir" ] ; then
	outdir=`dirname $video`
fi

cd ..

./inference_v2_seg.py --model pothole-segmentron-deeplabv3+-resnet101-basic-data-aug-img_size-540-540-2a-2021-11-04_11.02.00-WD-0.0001-BS-4-LR-0.0001-0.001-epoch-16-valid_loss-0.0402.pth --dataset-dir ./fake-dataset --class a,b,c --output-dir "$outdir"/baseline-deeplabv3+-resnet101 --video "$video"
