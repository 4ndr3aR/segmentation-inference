# Segmentation Inference
> A fairly customizable Python script capable of loading Pytorch models and performing semantic segmentation inference on both video and images.

![pothole-gallery](https://gitlab.com/4ndr3aR/pothole-mix-segmentation/-/raw/main/pics/gallery-3.jpg)

## Install

Clone this repository with:

`git clone https://gitlab.com/4ndr3aR/segmentation-inference.git`

then create a new virtualenv with:

`python3.8 -m venv /tmp/segmentation_inference`

source the newly created virtual env with:

`source /tmp/segmentation_inference/bin/activate`

and then run:

`pip install -r requirements.txt`

## How to use

To use this code, please create a `models` subdirectory and place your pretrained models there.

### Inference on videos

To test the baseline model, run this command (sorry for the model name):

```
./inference_v2_seg.py --model pothole-segmentron-deeplabv3+-resnet101-basic-data-aug-img_size-540-540-2a-2021-11-04_11.02.00-WD-0.0001-BS-4-LR-0.0001-0.001-epoch-16-valid_loss-0.0402.pth --dataset-dir ./fake-dataset --class a,b,c --output-dir /tmp/pothole-test --video /tmp/VID_20211031_162912-best-pothole-test-set-video-ever.mp4
```

To test one of the Team 1 models against a video, run this command:

```
./inference_v2_seg.py --arch create_model_manet_320_effb5 --model models/weights_smp_manet_320_effb5_cecv_01_last.pt --dataset-dir ./fake-dataset --class a,b,c --output-dir /tmp/pothole-test --video /tmp/VID_20211031_162912-best-pothole-test-set-video-ever.mp4
```

Or just run one of the existing scripts...

```
cd scripts
./unetpp-inference-video.sh /tmp/VID_20211031_162912-best-pothole-test-set-video-ever.mp4 /tmp/pothole-test/
```

### Inference on images

To perform batch inference on a given directory:

```
cd scripts
./inference-dir.sh manet /tmp/pothole-images /tmp/output-images-manet
```

