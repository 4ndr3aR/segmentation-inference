#!/usr/bin/env python3

import os
import re

import numpy as np
from pathlib import Path
import time
import sys

import argparse

from enum import Enum

import datetime as dt

from argument_parser import define_boolean_argument
from argument_parser import var2opt

from PIL import Image

# for cv2 video reader (perfectly fine)
import cv2

# for ffmpeg video writer (cv2 one sucks)
import shlex
import subprocess as sp

from utils import timing
from utils import curr_date_time
from utils import current_milli_time
from utils import fps_data
from utils import calc_and_show_fps

from overlay import overlay

args = None
env  = dict()

_fps_data = fps_data()

def do_show_fps(txt):
	print(f'{txt}', end='')

_fps_data.show_fps_text    = ''
_fps_data.show_fps_textual = True
_fps_data.show_fps_textual_func = do_show_fps
#_fps_data.show_fps_graphic = args.show_fps
#_fps_data.show_fps_graphic_func = do_show_fps

def check_output_dir(outdir):
	output_dir = Path(outdir)

	if not output_dir.exists():
		print(f'Creating directory: {output_dir}')
		output_dir.mkdir(exist_ok=True, parents=True)
	if output_dir.exists() and not output_dir.is_dir():
		print(f'Error: {output_dir} already exists and looks like it\'s a file! Quitting...')
		sys.exit(0)
	return

def setup_video_writer(args, img_size, fps, debug=True):

	output_dir = Path(args.output_dir)
	check_output_dir(output_dir)

	output_fn = output_dir / (str(Path(args.video).stem) + '-inference.mp4')
	ffmpeg_prefix = f'ffmpeg -y -s '
	ffmpeg_suffix = f' -pixel_format bgr24 -f rawvideo -r {fps} -i pipe: -vcodec libx264 -pix_fmt yuv420p -crf 24 {output_fn}'
	if args.rotatevideo != 'False':
		img_idxs = (1, 0)
	else:
		img_idxs = (0, 1)

	if debug:
		process = sp.Popen(shlex.split(f'{ffmpeg_prefix}{img_size[img_idxs[0]]}x{img_size[img_idxs[1]]}{ffmpeg_suffix}'), stdin=sp.PIPE)
	else:
		stdout = sp.DEVNULL
		stderr = sp.DEVNULL
		process = sp.Popen(shlex.split(f'{ffmpeg_prefix}{img_size[img_idxs[0]]}x{img_size[img_idxs[1]]}{ffmpeg_suffix}'), stdin=sp.PIPE, stdout=stdout, stderr=stderr)


	print(f'Writing output video with resolution: {img_size} to: {output_fn}')

	return process


#@timing
def do_overlay(img, mask, img_fn=None, output_dir=None, process=None, debug=False):


	if debug:
		print(f'Received img: {img.shape} - mask: {mask.shape}')
		print(f'Non-zero values: {np.count_nonzero(mask)} - mask: {mask.shape} {mask}')

	if output_dir is None:
		output_dir = '.'

	if img_fn is not None:
		out_fn     = f'{Path(img_fn).name.replace(".", "-")}.png'
		print(f'Saving image: {out_fn}')
		pred_fn    = Path(output_dir) / ('pred-' + out_fn)
		rgbpred_fn = Path(output_dir) / ('rgbpred-' + out_fn)
		mask.save(pred_fn)
		mask_img = Image.fromarray(np.uint8(mask))
		mask_img.save(rgbpred_fn)

	np_img = np.array(img)
	np_img = overlay(np_img, mask, alpha=0.5)
	np_img = Image.fromarray(np_img)

	if img_fn is not None:
		out_fn = Path(output_dir) / ('blend-' + out_fn.replace('.png', '.jpg'))
		print(f'Saving image: {out_fn}')
		np_img.save(out_fn)
	if args.video:
		np_img = np.array(img)
		np_img = overlay(np_img, mask, alpha=0.5)
		process.stdin.write(np.asarray(np_img))
		calc_and_show_fps(_fps_data)



def argument_parser():
	global args

	parser = argparse.ArgumentParser(description='Image Segmentation Inference with Fast.ai v2 and SemTorch')

	parser.add_argument('--img', default=''			, help='the image to use for inference')
	parser.add_argument('--dir', default=''			, help='scan this directory and perform inference on all the images')
	parser.add_argument('--output-dir', default=''		, help='put all output (images/videos) in this directory')
	parser.add_argument('--video', default=''		, help='the video to use for overlaying')
	parser.add_argument('--masks-video', default=''		, help='the video containing the masks to use for overlaying')
	define_boolean_argument(parser, *var2opt('rotatevideo')	, 'rotate the video 90° after inference (invert array idxs while saving)')
	parser.add_argument('--crop-src', default=''		, help='crop source image(s)/video before inference (e.g. x,y,w,h -> 0,540,1920,540 = bottom half of 1920x1080 img)')

	args = parser.parse_args()
	print(args)

	if args.crop_src:
		env['crop_src'] = args.crop_src.split(',')
		env['crop_src'] = [int(i) for i in env['crop_src']]
		print(f'Using the following source crop before performing inference: {env["crop_src"]}')

	if args.img:
		print(f'Performing overlay on image: {args.img}')
	elif args.dir:
		print(f'Performing overlay on all images in: {args.dir}')
		env['fns'] = get_image_filenames(args.dir)
	elif args.video:
		print(f'Performing overlay on video: {args.video} and masks video: {args.masks_video}')


if __name__ == '__main__':

	debug = False

	argument_parser()

	if args.video:
		cap      = cv2.VideoCapture(args.video)
		if args.masks_video:
			mcap	= cv2.VideoCapture(args.masks_video)
		else:
			print(f'Masks video parameter --masks-video has not been specified, nothing to do. Exiting...')
			sys.exit(0)
		img_size = [int(mcap.get(cv2.CAP_PROP_FRAME_HEIGHT)), int(mcap.get(cv2.CAP_PROP_FRAME_WIDTH))]
		fps      = int(mcap.get(cv2.CAP_PROP_FPS))
		process  = setup_video_writer(args, img_size, fps)
		while cap.isOpened() and mcap.isOpened:
			read_correctly, frame = cap.read()
			mread_correctly, mask = mcap.read()
			if not read_correctly or not mread_correctly:
				break
			do_overlay(frame, mask, process=process, debug=False)
	else:
		print(f'Video parameter --video has not been specified, nothing to do. Exiting...')
		sys.exit(0)


