#!/usr/bin/env python3

import os
import re

import numpy as np
from pathlib import Path
import time
import sys

import pickle

import pandas as pd

import argparse

from enum import Enum

import datetime as dt

import torch
from torch import Tensor

from torchvision import transforms

from fastai import torch_core
from fastai import *
from fastai.vision import *
from fastai.learner import *
from fastai.vision.core import *
from fastai.torch_core import *
from fastai.torch_core import to_np
from fastai.torch_core import defaults
from fastai.torch_core import distrib_barrier
from fastai.torch_core import TensorBase, TensorImage, TensorMask

from fastcore.transform import ItemTransform

from fastprogress import fastprogress
fastprogress.printing = lambda: False


from semtorch import get_segmentation_learner

from utils.argument_parser import define_boolean_argument
from utils.argument_parser import var2opt

from segmentation_inference.segmentation_inference import *

from PIL import Image

# for cv2 video reader (perfectly fine)
import cv2

# for ffmpeg video writer (the cv2 one sucks)
import shlex
import subprocess as sp


from utils.utils import curr_date_time
from utils.utils import current_milli_time
from utils.utils import fps_data
from utils.utils import calc_and_show_fps

from utils.overlay import overlay

# Examples of command line:
# ./inference_v2_seg.py --model pothole-segmentron-deeplabv3+-resnet101-basic-data-aug-img_size-540-540-2a-2021-11-04_11.02.00-WD-0.0001-BS-4-LR-0.0001-0.001-epoch-16-valid_loss-0.0402.pth --dataset-dir ./fake-dataset --classes a,b,c --video test-set-videos/VID_20211031_162912-best-pothole-test-set-video-ever.mp4 # --crop-src 0,540,1920,540


args = None
env  = dict()

_fps_data = fps_data()

def do_show_fps(txt):
	print(f'{txt}', end='')

_fps_data.show_fps_text    = ''
_fps_data.show_fps_textual = True
_fps_data.show_fps_textual_func = do_show_fps
#_fps_data.show_fps_graphic = args.show_fps
#_fps_data.show_fps_graphic_func = do_show_fps

#TensorMultiCategory.register_func(Tensor.__getitem__, TensorMultiCategory, TensorBBox)
TensorImage.register_func(Tensor.__getitem__, TensorImage, TensorMask)

def get_image_filenames(directory):
	jpg_fns = Path(directory).glob(f'*.jpg')
	jpg_fns_list = [] ; [jpg_fns_list.append(fn) for fn in jpg_fns]
	jpeg_fns = Path(directory).glob(f'*.jpeg')
	jpeg_fns_list = [] ; [jpeg_fns_list.append(fn) for fn in jpeg_fns]
	png_fns = Path(directory).glob(f'*.png')
	png_fns_list = [] ; [png_fns_list.append(fn) for fn in png_fns]
	all_fns = jpg_fns_list + jpeg_fns_list + png_fns_list
	print(f'Performing inference on these ({len(all_fns)}) files: {[fn.name for fn in all_fns]}')
	return all_fns

def get_num_gpus(debug=False):
    from subprocess import check_output
    # torch.cuda.device_count() works strange :)
    n_gpu = check_output(['nvidia-smi', '--list-gpus']).decode('UTF-8').count('\n')
    if debug:
        print(f'nvidia-smi found {n_gpu} GPUs')
    return int(n_gpu)


def setup_dataparallel(cuda_device, debug=False):
	n_gpu = get_num_gpus(debug=debug)
	if n_gpu > 1:
		import os
		os.environ['CUDA_VISIBLE_DEVICES'] = '0,1'
		os.environ['CUDA_DEVICE_ORDER']    = 'PCI_BUS_ID'

		#from fastai.distributed import *
		#cuda_device = 0
		torch.cuda.set_device(str(cuda_device))
	return n_gpu

def check_output_dir(outdir):
	output_dir = Path(outdir)

	if not output_dir.exists():
		print(f'Creating directory: {output_dir}')
		output_dir.mkdir(exist_ok=True, parents=True)
	if output_dir.exists() and not output_dir.is_dir():
		print(f'Error: {output_dir} already exists and looks like it\'s a file! Quitting...')
		sys.exit(0)
	return


def setup_video_writer(args, model_img_size, debug=True):

	output_dir = Path(args.output_dir)
	check_output_dir(output_dir)

	output_fn = output_dir / (str(Path(args.video).stem) + '-inference.mp4')
	ffmpeg_prefix = f'ffmpeg -y -s '
	ffmpeg_suffix = f' -pixel_format bgr24 -f rawvideo -r 60 -i pipe: -vcodec libx264 -pix_fmt yuv420p -crf 24 {output_fn}'
	if args.rotatevideo != 'False':
		img_idxs = (1, 0)
	else:
		img_idxs = (0, 1)

	if debug:
		process = sp.Popen(shlex.split(f'{ffmpeg_prefix}{model_img_size[img_idxs[0]]}x{model_img_size[img_idxs[1]]}{ffmpeg_suffix}'), stdin=sp.PIPE)
	else:
		stdout = sp.DEVNULL
		stderr = sp.DEVNULL
		process = sp.Popen(shlex.split(f'{ffmpeg_prefix}{model_img_size[img_idxs[0]]}x{model_img_size[img_idxs[1]]}{ffmpeg_suffix}'), stdin=sp.PIPE, stdout=stdout, stderr=stderr)


	print(f'Writing output video to: {output_fn}')

	return process



def do_inference_seg_impl(learn, img, device='cpu', n_gpu=1, debug=False):
	start = time.time()
	if debug:
		print(type(img))

	if debug:
		print(f'Performing inference on an image with this shape {img.shape} - mode: {img.mode} - type: {type(img)}...')
	if device != 'cpu':
		img = img.convert('RGB')
		print(f'Transformed image into Tensor with shape: {img.shape} and type: {type(img)}...')

		if n_gpu > 1:
			ctx = learn.parallel_ctx if n_gpu else None
			gpu = None
        
			with partial(ctx, gpu)():   # distributed traing requires "-m fastai2.launch"
				print(f"Performing {ctx.__name__} inference on GPU {list(range(n_gpu))} - torch.cuda.device_count(): {torch.cuda.device_count()}")
				raw_pred = learn.predict(img)
		else:
			print(f'Performing inference on device: {device}')
			raw_pred = learn.predict(img)
	else:
		img = np.array(img)
		if False:							# rotate input img - TODO: make this an argument
			img = np.transpose(img, (1, 0, 2))
			rotatedimg = Image.fromarray(np.uint8(img))
			rotatedimg.save('rotatedimg.png')
			print(type(img), img.shape)
		img = torch.from_numpy(img)
		raw_pred = learn.predict(img)
	end = time.time()
	prediction = raw_pred[0]
	if debug:
		print(80*'-')
		print(f'Raw prediction            : {type(raw_pred)} - {raw_pred[0].shape} - {raw_pred[1].shape} - {raw_pred}')
		print(f'Prediction label          : {type(prediction)} - {prediction.shape} - {prediction}')
		print(f'Elapsed time for inference: {end - start}')
		print(80*'-')
	return prediction, raw_pred


def do_inference_seg(learn, input_img, process=None, output_dir=None, debug=False):
	# ---------
	# This function produces grayscale masks as PNG files. When batch inference is over, simply run:
	# ---------
	# depthai-python/examples/custom-scripts/segmentation-mask-change-color.py -class1 0,0,0-255,0,0 -class2 1,1,1-0,0,255 -class3 2,2,2-0,255,0 -class4 3,3,3-44,0,0 -class5 4,4,4-185,26,255 -class6 5,5,5-0,211,255 -class7 6,6,6-0,88,0 -class8 7,7,7-255,255,255 --no-dry-run
	# ---------

	img_fn = None
	if isinstance(input_img, str) or isinstance(input_img, Path):
		if debug:
			print(f'Opening image: {input_img}')
		img_fn = input_img
		img    = load_image(input_img)
		if debug:
			print(f'Opened image {input_img} with size: {img.shape}')
	elif isinstance(input_img, np.ndarray):
		img    = input_img
		if debug:
			print(f'Received image {type(img)} with size: {img.shape}')
		img     = Image.fromarray(img)
	else:
		print(f'Input image in unknown format ({type(input_img)}). Exiting...')
		return

	if 'crop_src' in env:
		if debug:
			print(f'env[crop_src]: {env["crop_src"]}')
		x = env['crop_src'][0] # [0, 540, 1920, 540]
		y = env['crop_src'][1]
		w = env['crop_src'][2]
		h = env['crop_src'][3]
		img = img.crop((x, y, x+w, y+h))
		if debug:
			print(f'Performing inference on an image with this shape {img.shape} - type: {type(img)}...')

	img_s = img.resize(model_img_size)
	if debug:
		print(f'New image size: {img.shape}')

	prediction, raw_pred = do_inference_seg_impl(learn, img_s, debug=debug)

	do_inference_seg_post(img, prediction, raw_pred, img_fn=img_fn, process=process, output_dir=output_dir, debug=debug)


def do_inference_seg_post(img, prediction, raw_pred, img_fn=None, output_dir=None, process=None, debug=False):
	#class_colors = [[0,0,0], [255,0,0], [0,0,255], [0,255,0], [255,255,0], [0,255,255]]
	#class_colors = [[0,0,0], [0,0,255], [255,0,0], [0,255,0], [255,255,0], [0,255,255]]
	class_colors = []
	if '[' in args.classes:								# the user specified custom colors for classes via command line
		for cl in env['classes']:
			class_colors.append(env['classes'][cl])
	else:
		class_colors = [[0,0,0], [255,0,0], [0,0,255], [0,255,0], [255,255,0], [0,255,255]]

	class_colors = np.asarray(class_colors, dtype=np.uint8)

	predicted_mask = prediction
	predicted_mask = (to_np(predicted_mask))

	if debug:
		print(f'Non-zero values: {np.count_nonzero(predicted_mask)} - predicted_mask: {predicted_mask.shape} {predicted_mask}')

	predicted_mask = Image.fromarray(np.uint8(predicted_mask))

	colored_mask = np.take(class_colors, predicted_mask, axis=0)

	if output_dir is None:
		output_dir = '.'

	if img_fn is not None:
		out_fn     = f'{Path(img_fn).name.replace(".", "-")}.png'
		print(f'Saving image: {out_fn}')
		pred_fn    = Path(output_dir) / ('pred-' + out_fn)
		rgbpred_fn = Path(output_dir) / ('rgbpred-' + out_fn)
		predicted_mask.save(pred_fn)
		colored_mask_img = Image.fromarray(np.uint8(colored_mask))
		colored_mask_img.save(rgbpred_fn)

	np_img = np.array(img)
	np_img = overlay(np_img, colored_mask, alpha=0.5)
	np_img = Image.fromarray(np_img)

	if img_fn is not None:
		out_fn = Path(output_dir) / ('blend-' + out_fn.replace('.png', '.jpg'))
		print(f'Saving image: {out_fn}')
		np_img.save(out_fn)
	if args.video:
		img_s    = img.resize(model_img_size)
		np_img_s = np.array(img_s)
		#np_img_s= show_deeplabv3p(np.array(np_img_s), colored_mask, alpha=0.5)
		np_img_s = overlay(np_img_s, colored_mask, alpha=0.5)
		process.stdin.write(np.asarray(np_img_s))
		calc_and_show_fps(_fps_data)


def get_classes(args_classes, debug=False):
	if debug:
		print(20*'-', args_classes)
	if args_classes != '':
		env_classes = args_classes.split(',')
		if '[' in args_classes:
			classes_copy = env_classes
			env_classes = dict()
			for cl in classes_copy:
				if debug:
					print(cl)
				clname = cl.split('[')[0]
				if debug:
					print(clname)
				env_classes[clname] = [int(i) for i in cl.split('[')[1].replace(']','').split(';')]
				if debug:
					print(env_classes)
		print(f'Using the following classes: {env_classes}')
		return env_classes
	else:
		print(f'No classes provided, quitting...')
		sys.exit(0)

def argument_parser():
	global args

	parser = argparse.ArgumentParser(description='Image Segmentation Inference with Fast.ai v2 and SemTorch')

	parser.add_argument('--model-name'			, help='the model to load for inference (can be both a .pkl or .pth model)')
	parser.add_argument('--classes', default=''		, help='classes of the problem (e.g. asphalt,pothole,crack)')
	parser.add_argument('--img', default=''			, help='the image to use for inference')
	parser.add_argument('--dir', default=''			, help='scan this directory and perform inference on all the images')
	parser.add_argument('--output-dir', default=''		, help='put all output (images/videos) in this directory')
	parser.add_argument('--video', default=''		, help='the video to use for inference')
	define_boolean_argument(parser, *var2opt('rotatevideo')	, 'rotate the video 90° after inference (invert array idxs while saving)')
	define_boolean_argument(parser, *var2opt('test_set')	, 'don\'t use a test image, use the whole test set in --dataset-dir')
	parser.add_argument('--export-pkl-model', default=None	, help='if present, load the .pth model + create a DataLoader object for it from --dummy-dataset-dir and export a .pkl model')
	parser.add_argument('--dataset-dir', default=None	, help='if present, use this dataset to create a DataLoader object for the Learner')
	parser.add_argument('--crop-src', default=''		, help='crop source image(s)/video before inference (e.g. x,y,w,h -> 0,540,1920,540 = bottom half of 1920x1080 img)')
	parser.add_argument('--arch'				, help='create a custom architecture from module `arch` -- the module must be in the current directory and have the following API: `def create_model(device="cuda:0")` -- provide the module name without .py extension')
	define_boolean_argument(parser, *var2opt('save_image')	, 'save the resized image before feeding it to the network')

	args = parser.parse_args()
	print(args)
	if args.arch:
		print(f'Creating custom architecture from module: {args.arch}')
	if args.model_name:
		print(f'Loading model: {args.model_name}')
	else:
		print(f'No model provided, quitting...')
		sys.exit(0)

	env['classes'] = get_classes(args.classes)

	if args.crop_src:
		env['crop_src'] = args.crop_src.split(',')
		env['crop_src'] = [int(i) for i in env['crop_src']]
		print(f'Using the following source crop before performing inference: {env["crop_src"]}')

	if args.img:
		print(f'Model will perform inference on image: {args.img}')
	elif args.dir:
		print(f'Model will perform inference on all images in: {args.dir}')
		env['fns'] = get_image_filenames(args.dir)
	elif args.video:
		print(f'Model will perform inference on video: {args.video}')

	elif args.test_set:
		print(f'Model will perform inference on the test set under: {args.dataset_dir}')
	else:
		print(f'No image provided, quitting...')
		sys.exit(0)
	if args.export_pkl_model != None:
		print(f'Exporting model to: {args.export_pkl_model}')


def create_learner(model_name, dataset_dir, env, device='cpu', debug=False):
	if args.dataset_dir:
		print(f'A dataloader from: {args.dataset_dir} will be built')
	else:
		print(f'No dataset provided, unable to create a learner and load a PTH model, quitting...')
		sys.exit(0)

	model_img_size, model_depth = get_model_params_from_filename(model_name)
	if model_img_size is not None:
		env['img_size']	= tuple(model_img_size)
	else:
		env['img_size']	= (224, 224)

	env['path']		= Path(dataset_dir)

	env['datasets']		= [ 'dummy' ]

	env['orig_img_size']	= (1080, 1920)
	env['bs']		= 16
	env['num_workers']	= env['bs']
	env['segmentation_type']= "Semantic Segmentation"
	env['arch']		= "deeplabv3+"
	if model_depth is not None:
		env['backbone']	= "resnet" + str(model_depth)
	else:
		env['backbone']	= "resnet18"
	env['wd']		= 1e-4
	env['no-summary']	= True		# don't print summary in init_learner()
	env['no-show-batch']	= True		# don't show_batch() in init_learner() (because we'll probably run headless)
	#env['classes']		= [ 'asphalt', 'crack', 'pothole' ]
	env['n_classes']	= len(env['classes'])

	print('Calling init_learner()...')
	learn = init_learner(env, splitter=TestGrandGrandParentSplitter, valid_dir='testing')

	return learn

def load_pth_model(model_name, dataset_dir, env, device='cpu', debug=False):		# the pth model needs also a dataloader to work

	learn = create_learner(model_name, dataset_dir, env, device=device, debug=debug)

	learn.load(str(model_name).replace('.pth',''), device=device)
	if hasattr(learn, 'dls'):
		print(f'type(learn.dls) = {type(learn.dls)}')
		print(f'learn.dls       = {learn.dls}')
		if debug:
			dump(learn.dls)

	if device != 'cpu':
		print(f'Moving model to CUDA...')
		learn.model = learn.model.cuda()
		if hasattr(learn, 'dls'):
			print(f'Moving dataloader to CUDA...')
			learn.dls.to('cuda')		# https://forums.fast.ai/t/very-slow-inference-using-get-preds-in-fastai2/70841/3

	return learn

def load_learner_to_gpu(fname, cuda_device='cuda:0', cpu=False, pickle_module=pickle, debug=False):
    "Load a `Learner` object in `fname`, optionally putting it on the `cpu`"
    distrib_barrier()
    learn = torch.load(fname, map_location='cpu' if cpu else cuda_device, pickle_module=pickle_module)
    if hasattr(learn, 'to_fp32'): learn = learn.to_fp32()
    if cpu: learn.dls.cpu()
    if debug:
        print(learn.model)
    return learn

def load_pkl_to_gpu(fname, cuda_device='cuda:0'):
    "Load a `Learner` object in `fname`, optionally putting it on the `cpu`"
    distrib_barrier()
    learn = load_learner(fname)
    return learn

def create_custom_arch(module_name, cuda_device='cuda:0', debug=False):
	"Create custom arch calling create_model() from a custom python module (passed at runtime)"
	import importlib

	custom_module = importlib.import_module(module_name)
	model = custom_module.create_model(device=cuda_device)
	if debug:
		print(f'Successfully loaded model: {model}')
	return model

def load_custom_weights_into_learner(fname, learn, cuda_device='cuda:0', debug=False):
	"Load pytorch weights on a custom arch"
	distrib_barrier()
	weights = torch.load(fname, map_location=cuda_device)
	learn.model.load_state_dict(weights)
	return learn.model

def load_pkl_model(fn, device='cpu', debug=False):		# nothing else needed, just the pkl file
	load_to_cpu = True
	if device != 'cpu':
		print(f'Loading the model to GPU - torch.cuda.current_device(): {torch.cuda.current_device()}')
		load_to_cpu = False
	learn = load_learner_to_gpu(fn, cuda_device=device, cpu=load_to_cpu, debug=debug)	# https://forums.fast.ai/t/make-inference-run-on-the-gpu/63912
	if not load_to_cpu:
		learn.model = learn.model.to(device)
		learn.dls.to(device)				# https://forums.fast.ai/t/very-slow-inference-using-get-preds-in-fastai2/70841/3

	print(80*'-')
	print(f'Loaded pkl model: {fn}')
	print(80*'-')
	return learn

def load_model(args, env, debug=False):
	learn = None

	if 'pkl' in args.model_name.lower():
		learn = load_pkl_model(Path(args.model_name))						# nothing else needed, just the pkl file
	elif 'pth' in args.model_name.lower():
		learn = load_pth_model(Path(args.model_name), args.dataset_dir, env, debug=debug)	# the pth model needs also a dataloader to work
	else:
		print(f'Model in unknown format...')
		sys.exit(0)

	if debug:
		dump(learn)
	if hasattr(learn, 'dls'):
		if debug:
			dump(learn.dls)
			# Still not clear if this stuff has to be removed:
			# https://forums.fast.ai/t/fastaiv2-segmentation-inference-with-arbitrary-image-sizes/83102/2
			print(f'After item  pipeline: {[f.name for f in learn.dls.after_item.fs]}')
			print(f'After batch pipeline: {[f.name for f in learn.dls.after_batch.fs]}')
		#print(f'Changing learn.dls.after_item default resize transform: {learn.dls.after_item}')
		print([f.name for f in learn.dls.after_item.fs])
		for idx, f in enumerate(learn.dls.after_item.fs):
			if 'resize' in f.name.lower():
				print(f'Transform resize: {f}')
				print(f'Transform resize default size: {f.size}')

	return learn

def dump(obj):
	blacklist = [ '_after', '_call', '_get', '_is_showable', '_name', '_repr_pretty_', 'default', 'train_setup' ]
	for attr in dir(obj):
		print(f'--- attr: {attr}')
		found = False
		for bl in blacklist:
			if bl in attr:
				found = True
		if not found:
			if hasattr(obj, attr):
				print("obj.%s = %r" % (attr, getattr(obj, attr)))

from fastcore.basics import patch
from fastcore.transform import Pipeline
from fastai.data.load import DataLoader
@patch
def remove(self:Pipeline, t):
    "Remove an instance of `t` from `self` if present"
    for i,o in enumerate(self.fs):
        if isinstance(o, t.__class__): self.fs.pop(i)
@patch
def set_base_transforms(self:DataLoader):
    "Removes all transforms with a `size` parameter"
    attrs = ['after_item', 'after_batch']
    for i, attr in enumerate(attrs):
        tfms = getattr(self, attr)
        for j, o in enumerate(tfms):
            if hasattr(o, 'size'):
                tfms.remove(o)
        setattr(self, attr, tfms)

def get_model_params_from_filename(model_name, debug=False):
	model_fn_tokens = str(model_name).split('-')
	model_img_size  = model_depth = None
	for idx, tok in enumerate(model_fn_tokens):
		if 'img_size' in tok.lower():
			model_img_size = (int(model_fn_tokens[idx+1]), int(model_fn_tokens[idx+2]))
		if 'resnet' in tok.lower():
			model_depth    = int(model_fn_tokens[idx].lower().replace('resnet', ''))
	if model_img_size is None:
		#model_img_size = re.findall("ai", txt)
		#'s:.*\([0-9][0-9][0-9]\).*:\1:g'
		match = re.search("(\d+)", model_name)
		if match:
			model_img_size = match.groups()
			if len(model_img_size) == 1:
				model_img_size = (model_img_size[0], model_img_size[0])

	model_img_size = [int(i) for i in model_img_size]
	if debug:
		print(f'model_img_size: {model_img_size} - model_depth: {model_depth}')
	return model_img_size, model_depth

def rescale_numpy_image(img):
	rescaled = (255.0 / img.max() * (img - img.min())).astype(np.uint8)
	return rescaled

def get_inv_tfm_mapping(debug=False):
	tfm = TargetMaskConvertTransform()
	if debug:
		print(tfm.mapping)
	inv_tfm_mapping = {v: k for k, v in tfm.mapping.items()}
	if debug:
		print(inv_tfm_mapping)
	return inv_tfm_mapping

def pil_image_from_one_hot_encoded_pred_tensors(img, debug=False):
	# Because learn.get_preds() spits one-hot encoded tensors of (BS*CODES*W*H) size! Solution found here:
	# https://colab.research.google.com/github/muellerzr/Practical-Deep-Learning-for-Coders-2.0/blob/master/Computer%20Vision/04_Segmentation.ipynb
	img_arx = img.argmax(dim=0)
	if debug:
		print(f'img_arx		:	{type(img_arx)}	{img_arx.shape}')

	img_arx = img_arx.numpy()
	if debug:
		print(f'img_arx		:	{type(img_arx)}	{img_arx.shape}')

	img_rescaled = rescale_numpy_image(img_arx)

	if debug:
		print(f'img_rescaled	:	{type(img_rescaled)}	{img_rescaled.shape}')

	pimg = PILImage.create(img_rescaled)
	return pimg

def add_palette_to_pil_image(img, mapping, debug=False):
	#0: (0, 0, 255), 1: (255, 0, 0), 2: (0, 255, 0), 3: (0, 0, 44), 4: (255, 26, 185), 5: (255, 211, 0), 6: (0, 88, 0), 7: (255, 255, 255)
	palette_list = []
	for k, v in inv_tfm_mapping.items():
		for itm in v:
			palette_list.append(itm)
	if debug:
		print(f'palette_list: {len(palette_list)} - {palette_list}')

	img_palette = img.convert('P')
	img_palette.putpalette(palette_list)
	return img_palette





if __name__ == '__main__':

	os.environ['CUDA_VISIBLE_DEVICES'] = '0'
	defaults.device = torch.device('cpu')

	debug = False
	learn = None

	argument_parser()
	model_img_size, model_depth = get_model_params_from_filename(Path(args.model_name).stem)

	if not args.arch:
		# Load a Pytorch/Fast.ai pretrained model, in pkl or pth format
		print(f'Images will be resized to native model size: {model_img_size}')

		learn = load_model(args, env, debug=debug)
	else:
		# Create a custom arch and load pretrained weights (pt format) into it
		learn = create_learner(args.arch, args.dataset_dir, env, device='cuda:0', debug=False)
		learn.model = create_custom_arch(args.arch, cuda_device='cuda:0', debug=False)
		load_custom_weights_into_learner(args.model_name, learn, cuda_device='cuda:0', debug=False)


	if args.img:
		do_inference_seg(learn, args.img, debug=debug)
	elif args.dir:
		output_dir = Path(args.output_dir)
		check_output_dir(output_dir)

		for fn in env['fns']:
			do_inference_seg(learn, fn, output_dir=output_dir, debug=debug)
	elif args.video:
		cap	= cv2.VideoCapture(args.video)
		process	= setup_video_writer(args, model_img_size)
		while cap.isOpened():
			read_correctly, frame = cap.read()
			if not read_correctly:
				break
			do_inference_seg(learn, frame, process=process, debug=False)

	elif args.test_set:
		print(f'Getting predictions on the test set, please wait...')
		preds_path  = None
		tgts_path   = None

		inv_tfm_mapping = get_inv_tfm_mapping(debug=debug)

		predictions = learn.get_preds(ds_idx=1, with_decoded=True, save_preds=preds_path, save_targs=tgts_path)
		print(f'predictions	:	{type(predictions)}	{len(predictions)}')
		print(f'predictions[0]	:	{type(predictions[0])}	{len(predictions[0])}')
		print(f'predictions[1]	:	{type(predictions[1])}	{len(predictions[1])}')
		print(f'predictions[2]	:	{type(predictions[2])}	{len(predictions[2])}')
		for i in range(len(predictions[0])):
			img1  = predictions[0][i]
			msk   = predictions[1][i]
			img2  = predictions[2][i]
			print(f'img1		:	{type(img1)}	{img1.shape}')
			print(f'msk		:	{type(msk)}	{ msk.shape}')
			print(f'img2		:	{type(img2)}	{img2.shape}')

			pimg1		= pil_image_from_one_hot_encoded_pred_tensors(img1, debug=debug)
			pimg1_palette	= add_palette_to_pil_image(pimg1, inv_tfm_mapping, debug=debug)

			msk_rescaled	= rescale_numpy_image(msk.squeeze().numpy())
			print(f'msk_rescaled	:	{type(msk_rescaled)}	{msk_rescaled.shape}')
			pmsk		= PILMask.create(msk_rescaled)

			img2_rescaled	= rescale_numpy_image(img2.numpy())
			print(f'img2_rescaled	:	{type(img2_rescaled)}	{img2_rescaled.shape}')
			pimg2 = PILImage.create(img2_rescaled)
			pimg1.save('/home/ranieri/repos/segmentation_inference/preds/img1-'+str(i)+'.jpg')
			pimg1_palette.save('/home/ranieri/repos/segmentation_inference/preds/img1-'+str(i)+'.png')
			pmsk.save ('/home/ranieri/repos/segmentation_inference/preds/msk-' +str(i)+'.png')
			pimg2.save('/home/ranieri/repos/segmentation_inference/preds/img2-'+str(i)+'.jpg')
	else:
		print(f'Neither --img nor --test-set options have been specified, nothing to do. Exiting...')
		sys.exit(0)


